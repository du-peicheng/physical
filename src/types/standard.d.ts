declare namespace StandardMoudle {
  // 添加指标表单项数据
  interface IStandardAddForm {
    standardID: string; // 标准指标ID
    standardName: string; // 标准指标名称
    unit: string; // 指标单位
    upperLimit: number; // 参考上限
    lowerLimit: number; // 参考下限
    normalPrompt?: string; // 正常提示
    highPrompt?: string; // 偏高提示
    lowPrompt?: string; // 偏低提示
    isComparable?: boolean; // 是否比对
    counterpart?:string; // 对应指标标准
    affiliation?:string; // 所属机构
  }

  // 接口返回标准指标列表数据
  interface IStandardInfo extends IStandardAddForm {
    id?: number
    url?: string
  }

  interface IStandardList {
    total: number
    Standards: IStandardInfo[]
  }

  // 搜索表单项
  type ISearchState = Partial<IStandardAddForm>


  interface IQueryStandard {
    page?:number
    size?:number
  }

  // 导入excel表数据
  type IStandardAddsForm = IStandardAddForm[]


  interface IDatas {
    matching: string
    standard: string
    institution: string
  }
  
}
/*
 * @Author: cunFox 
 * @Date: 2023-11-18 09:00:34 
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-19 22:44:10
 */


// 预约接口数据类型
declare namespace appointmentModule {
  // --------------------------展示的数据接口
  // 预约用户信息
  interface IInfoData {
    id?: number;
    name: string;
    sex: string;
    tel: string;
    nId: string;
    content: string;
    organ: string;
    date: string;
    orderId: string | number;
    state: string;
  }
  // 查询状态选项
  interface IOptions {
    label: string;
  }
  // 查询的数据
  interface ISearch {
    value:string
    state:string
    date:s
  } 

  // ------------------------------返回的数据接口
  //   返回的用户信息
  interface IBackInfo {
    page?: number;
    size?: number;
    total?: number;
    tableData: IInfoData[];
  }

  //   返回的状态选项
  interface IBackOptions {
    options: IOptions[];
  }

  // ------------------------------
  
}

/*
 * @Author: cunFox
 * @Date: 2023-11-20 17:51:24
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-21 22:06:07
 */

declare namespace organizationModule {
  // -----------------------------请求数据接口
  // 机构数据
  interface IOrganData {
    organId?: number;
    organName: string;
    organBranch: string;
    city: string;
    organType: string;
    tel: string;
    enable: string;
    northern: string
    east: string
    capitalsName: string
    type: string
    avatar: string
    organUrl:string
  }
  // 查询数据
  interface IOrganSearch {
    organName: string;
    enable: string;
    city: string;
    organType: string;
    capitalsName: string;
  }
  // 删除数据类型
  interface IOrganDelete {
    operate:number
    organData:IOrganData[]
  }
  // 文件类型
  interface IUpdateOrganAvatar {
    path: string
    url: string
  }

  // 添加的数据类型
  interface IOrganAddData {
    organName: string;
    organBranch: string;
    city: string;
    organType: string;
    tel: string;
    enable: string;
    northern: string
    east: string
    capitalsName: string
    type: string
    avatar: string
    organUrl:string
  }

  interface ISourceData {
    id:number
    target:IBackSource
  }

  // -----------------------------返回数据接口

  interface IBackOrganData {
    page?: number;
    size?: number;
    total?: number;
    tableData: IOrganData[];
  }
  // 省份类型
  interface ICapital {
    id: number;
    region: string;
  }

// 城市类型
  interface ICity {
    cid: number;
    cities: string[];
  }
// 机构类型
  interface IOrgan {
    key: string;
    value: string;
    label: string;
  }

  interface IBAckTypeData {
    capital: ICapital[];
    city: ICity[];
    organ: IOrgan[];
  }

  interface IBackSource {
    date:string
    leisure:number
    busyness:number
  }
  
  
  interface IBackSourceList {
    source:IBackSource[]
  }
}

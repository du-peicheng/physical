declare namespace SelectModule {
    // 省份和城市列表
    interface IDataItem {
      value: string;
    }
    
    type IData = IDataItem[][];

    // 搜索的数据
    interface ISelectForm{
      name: string;
      province: string;
      cities: string;
      institution: string;
  }
    // table
    interface Itable
      {
        id: string;
        name: string;
        oldPrice: number;
        sellingPrice: number;
        cities: string;
        enable: string;
        quantity: number;
    }[];
    
    // 表格数据
    interface ISelectTable{
      total: number;
      table: {
          id: string;
          name: string;
          oldPrice: number;
          sellingPrice: number;
          cities: string;
          enable: string;
          quantity: number;
      }[];
  }
  }
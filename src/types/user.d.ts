
declare namespace UserModule{
    // 用户登录
    interface IUserForm{
        username:string,
        password:string
    }

    // 登录成功后 返回接口数据
    interface ApiUserLoginRet{
        userid:number,
        name:string,
        token:string,
        expirationTime?:number
    }

    // 全局状态中的类型 state
    interface IState{
        userinfo:ApiUserLoginRet
    }

      // 添加用户表单项数据
  interface IUserAddForm extends IUserForm {
    realname: string;
    sex: string;
    phone: string;
    birthday: string;
    avatar: string;
  }
}

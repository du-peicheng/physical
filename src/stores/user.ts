import { defineStore } from 'pinia'
import { userLoginApi } from '@/services/userApi'

const useUserStore = defineStore('user', {
  // 对于登录信息进行一次持久化操作
  persist: {
    storage: window.sessionStorage,
    paths: ['userinfo'],
  },
  state: () => ({
    userinfo: {
      userid: 0,
      name: '',
      token: '',
      expirationTime: 0, // 新增字段，用于存储过期时间
    }
  }) as UserModule.IState,
  actions: {
    // 设置用户登录后的基础信息，同时设置过期时间
    setUserInfo(user: UserModule.ApiUserLoginRet) {
      this.userinfo = {
        ...user,
        expirationTime: Date.now() + (24 * 60 * 60 * 1000), // 设置过期时间为 24 小时
        // expirationTime: Date.now() + (10000), //测试
      };
      this.userinfo = {
        ...user,
        expirationTime: Date.now() + (24 * 60 * 60 * 1000), // 设置过期时间为 24 小时
        // expirationTime: Date.now() + (10000), //测试
      };
    },
    // 用户退出
    logoutUserInfo() {
      this.userinfo = {
        userid: 0,
        name: '',
        token: '',
        expirationTime: 0,
      };
    },
    // 发起网络请求，进行用户登录验证
    // 一定要返回，需要在登录成功后，进行路由跳转
    async userLoginAction(userData: UserModule.IUserForm) {
      let ret = await userLoginApi(userData);
      if (ret.code === 0) {
        this.setUserInfo(ret.data);
      }
      return ret.code;
    },
  },
});


export default useUserStore;
import { post, get,put } from '@/utils/http'
import { objToQueryString } from '@/utils/toolsPjt'

/**
 * 添加标准指标
 * @param {object} standardData 添加标准指标表单项数据
 * @returns Promise<number>
 */
export const standardAddApi = (standardData: StandardMoudle.IStandardAddForm) => {
  return post<number>(`/api/standard/add`, standardData)
}

// 标准指标列表数据
export const standardListApi = (page = 1, query = {}, size = 10) => {
  if ('page' in query) {
    delete query.page
  }
  query = objToQueryString(query)
  return get<StandardMoudle.IStandardList>(`/api/standard/list?page=${page}&size=${size}${query}`)
}

// 查询给定指标
export const standardSearchApi = (standardSearchData?:StandardMoudle.ISearchState) => {
  if(!standardSearchData){
    return get<StandardMoudle.IStandardList>(`/api/standard/list?page=1&size=10`)
  } else {
    return get<StandardMoudle.IStandardList>(`/api/standard/search?page=1&${objToQueryString(standardSearchData)}`)
  }
}

// 批量删除
export const deleteAllstandardApi = (ids: number[]) => {
  return post<number>(`/api/standard/deleteAllstandard`, { ids })
}

// 单个删除
export const deleteStandardApi = (id: number) => {
  return post<number>(`/api/standard/deleteStandard`, { id })
}


// 根据指标id返回指标数据
export const getSidToInfoApi = (id: number) => {
  return get<StandardMoudle.IStandardInfo>(`/api/standard/${id}`)
}

// 根据用户id修改对应用户记录
export const standardEditApi = (id:number,standardData:StandardMoudle.IStandardInfo)=>{
  return put<number>(`/api/standard/${id}`,standardData)
}


/**
 * 导入excel表中数据
 * @param {object} standardData 添加标准指标表单项数据
 * @returns Promise<number>
 */
export const standardAddsApi = (standardDatas: StandardMoudle.IStandardAddsForm) => {
  return post<number>(`/api/standard/adds`, standardDatas)
}

import { post, get } from '@/utils/http'

/**
 * 获取省份城市信息
 * @returns Promise<>
 */
export const selectCityData = ()=>get('/api/select/province')

/**
 *  体检套餐搜索框搜索 
 *  @param {object}  selectForm 搜索表单数据
 *  @returns Promise<ISelectForm>
 */
export const selectData = (selectForm:SelectModule.ISelectForm)=>post('/api/select/submit',selectForm)


/**
 *  体检项搜索框搜索 
 *  @param {object}  selectForm 搜索表单数据
 *  @returns Promise<ISelectForm>
 */
export const selectProData = (selectForm:SelectModule.ISelectForm)=>post('/api/selectpro/submit',selectForm)
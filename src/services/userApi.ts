import { post, get,put } from '@/utils/http'


/**
 *  用户登录 
 *  @param {object}  userData 用户登录表单数据
 *  @returns Promise<IUserForm>
 */
export const userLoginApi = (userData:UserModule.IUserForm)=>post<UserModule.ApiUserLoginRet>('/api/user/login',userData)


/**
 * 用户退出登录
 * @returns Promise<IUserForm>
 */
export const userLogoutApi = ()=>get('/api/user/logout')
/*
 * @Author: cunFox 
 * @Date: 2023-11-18 09:20:52 
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-20 17:58:35
 */


import { get, post } from "@/utils/http";

/**
 * get请求,获取用户列表数据
 * @param page 页数
 * @param size 每页数据量
 * @returns Promise<IData<appointmentModule.IInfoData>>
 */
 export const infoTableApi = (page= 1,size = 10) => get<appointmentModule.IBackInfo>(`/api/appointment/infoList?page=${page}&size=${size}`)

/**
 * get请求,获取查询的状态选项
 * @returns Promise<IData<appointmentModule.IBackOptions>>
 */
export const infoOptionsApi = () => get<appointmentModule.IBackOptions>('/api/appointment/options')

/**
 * post请求，用于筛选用户
 * @param searchData appointmentModule.ISearch
 * @returns Promise<IData<number>>
 */
export const infoSearchApi = (searchData:appointmentModule.ISearch) => post<number>('/api/appointment/search',searchData)

/**
 * post请求，用于修改用户状态
 * @param userData appointmentModule.IInfoData
 * @returns Promise<IData<number>>
 */
export const infoStateChangeApi = (userData:appointmentModule.IInfoData) => post<number>('/api/appointment/state',userData)

/**
 * post请求，用于批量修改用户状态
 * @param userData appointmentModule.IInfoData
 * @returns Promise<IData<number>>
 */
export const infoStatesChangeApi = (userData:appointmentModule.IInfoData[]) => post<number>('/api/appointment/state',userData)


/**
 *  post请求，用于修改用户时间
 * @param userData appointmentModule.IInfoData
 * @returns Promise<IData<number>> 
 */
export const infoDateChangeApi = (userData:appointmentModule.IInfoData) => post<number>('/api/appointment/setDate',userData)

/**
 *  post请求，用于修改用户时间
 * @param userData appointmentModule.IInfoData
 * @returns Promise<IData<number>> 
 */
export const infoAddExcelApi = (userData:any) =>{
    console.log(userData);
    return post<number>('/api/appointment/addExcel',userData)
} 


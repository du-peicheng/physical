import { post, get } from '@/utils/http'
import { objToQueryString } from '@/utils/tools'



// 获取体检套餐表格数据
export const userListApi = (page = 1, query = {}, size = 10) => {
  if ('page' in query) {
    delete query.page
  }
  if('size' in query){
    delete query.size
  }
  query = objToQueryString(query)
  return get<SelectModule.ISelectTable>(`/api/table/data?page=${page}&size=${size}${query}`)
}

// 获取体检项表格数据
export const projectListApi = (page = 1, query = {}, size = 10) => {
  if ('page' in query) {
    delete query.page
  }
  if('size' in query){
    delete query.size
  }
  query = objToQueryString(query)
  return get<SelectModule.ISelectTable>(`/api/tableproject/data?page=${page}&size=${size}${query}`)
}

// 删除表格数据(体检套餐)
export const tableDelApi = (id:number)=>post('/api/table/del',id)

// 删除表格数据(体检项)
export const tableDelProApi = (id:number)=>post('/api/table/delPro',id)


// 获取体检项表格数据
export const projectApi=()=>get('/api/table/project')


// 提交体检套餐
export const addPackgeApi=(data:any)=>post('/api/table/addpackage',data)

// 提交体检项
export const addProjectApi=(data:any)=>post('/api/table/addproject',data)

// 批量删除体检套餐
export const reomvePackagesApi=(idList:Array<string>)=>post('/api/table/removepackages',idList)

// 批量删除体检项
export const reomveProjectsApi=(idList:Array<string>)=>post('/api/table/removeprojects',idList)
// 回显的请求
export const echoDialogFormData=(id:string)=>post('/api/table/echoData',id)
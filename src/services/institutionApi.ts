import { post, get,put } from '@/utils/http'
import { objToQueryString } from '@/utils/toolsPjt'

/**
 * 添加机构指标
 * @param {object} institutionData 添加机构指标表单项数据
 * @returns Promise<number>
 */
export const institutionAddApi = (institutionData: StandardMoudle.IStandardAddForm) => {
  return post<number>(`/api/institution/add`, institutionData)
}

// 机构指标列表数据
export const institutionListApi = (page = 1, query = {}, size = 10) => {
  if ('page' in query) {
    delete query.page
  }
  query = objToQueryString(query)
  return get<StandardMoudle.IStandardList>(`/api/institution/list?page=${page}&size=${size}${query}`)
}

// 查询给定指标
export const institutionSearchApi = (institutionSearchData?:StandardMoudle.ISearchState) => {
  if(!institutionSearchData){
    return get<StandardMoudle.IStandardList>(`/api/institution/list?page=1&size=10`)
  } else {
    return get<StandardMoudle.IStandardList>(`/api/institution/search?page=1&${objToQueryString(institutionSearchData)}`)
  }
}

// 批量删除
export const deleteAllInstitutionApi = (ids: number[]) => {
  return post<number>(`/api/institution/deleteAllInstitution`, { ids })
}

// 单个删除
export const deleteInstitutionApi = (id: number) => {
  return post<number>(`/api/institution/deleteInstitution`, { id })
}


// 根据指标id返回指标数据
export const getNidToInfoApi = (id: number) => {
  return get<StandardMoudle.IStandardInfo>(`/api/institution/${id}`)
}

// 根据用户id修改对应用户记录
export const institutionEditApi = (id:number,institutionData:StandardMoudle.IStandardInfo)=>{
  return put<number>(`/api/institution/${id}`,institutionData)
}


/**
 * 导入excel表中数据
 * @param {object} institutionDatas 添加机构指标表单项数据
 * @returns Promise<number>
 */
export const institutionAddsApi = (institutionDatas: StandardMoudle.IStandardAddsForm) => {
  return post<number>(`/api/institution/adds`, institutionDatas)
}


export const getMatchingDataApi = (id:number,name?: string,Iname?:string) => {
  let setquery = {id,name,Iname}
  let query = objToQueryString(setquery)
  return get<StandardMoudle.IDatas[]>(`/api/institution/data/${id}?${query}`)
}


export const addMappingApi = (id: number,data: StandardMoudle.IDatas[]) => {
  return post<number>(`/api/institution/addMapping/${id}`, data)
}


export const editMappingApi = (id: number,data: StandardMoudle.IDatas[])=>{
  return put<number>(`/api/institution/editMapping/${id}`,data)
}

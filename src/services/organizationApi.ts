import { get, post } from "@/utils/http";

/**
 * 
 * @param page 
 * @param size 
 * @returns 
 */
export const organTableApi = (page:number = 1,size:number = 10) => get<organizationModule.IBackOrganData>(`/api/organization/organList?page=${page}&size=${size}`)

/**
 * 
 * @returns 
 */
export const organTypeApi = () => get<organizationModule.IBAckTypeData>('/api/organization/typeData')

/**
 * 
 * @param data 
 * @returns 
 */
export const organSearchApi = (data:organizationModule.IOrganSearch) => post<number>('/api/organization/search',data)

/**
 * 
 * @param data 
 * @returns 
 */
export const organDeleteApi = (data:organizationModule.IOrganDelete) => post<number>('/api/organization/delete',data)

/**
 * 上传图片
 * @param formData 
 * @returns 
 */
export const upOrganAvatarApi =(formData: FormData) => post<organizationModule.IUpdateOrganAvatar>('/api/organization/uploadOrganAvatar', formData)

/**
 * 删除上传图片
 * @param path 
 * @returns 
 */
export const deleteOrganAvatarApi = (path: string) => post<number>('/api/organization/deleteOrganAvatar', { path })

/**
 * 
 * @param data 
 * @returns 
 */
export const addOrganDataAPi = (data:organizationModule.IOrganData) => post<number>('/api/organization/addData',data)

/**
 * 
 * @param data 
 * @returns 
 */
export const alterOrganDataAPi = (data:organizationModule.IOrganData) => post<number>('/api/organization/alterData',data)



export const getMessageSourceApi = (id:number) => post<organizationModule.IBackSourceList>('/api/organization/MessageSource',id)


export const addMessageSourceApi = (data:organizationModule.ISourceData) => post<number>('/api/organization/addSource',data)




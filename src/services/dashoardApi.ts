import { post, get } from '@/utils/http'

/**
 * 获取首页卡片数据
 * @returns Promise<>
 */
export const dashoardTotalApi = ()=>get('/api/dashord/card')

/**
 * 获取首页概况数据
 * @returns Promise<>
 */
export const dashoardOverviewsApi = ()=>get('/api/dashord/overviews')
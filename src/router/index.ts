import { createRouter, createWebHistory } from 'vue-router'
import checkUserLogin from './hooks/checkUserLogin'
import type { RouteRecordRaw } from "vue-router";

// 自动引入routes目录下面的所有的定义的子路由
const files = import.meta.glob('./routers/*.ts', { eager: true })
export const children = [] as RouteRecordRaw[]
for (let key in files) {
  let route = (files[key as keyof typeof files] as any).default
  if (route && (Array.isArray(route) || route.path)) {
    if (Array.isArray(route)) {
      children.push(...route)
    } else {
      children.push(route)
    }
  } else {
    console.error(`Invalid route structure in file: ${key}`)
  }
}


// 把首页手动调到children的第0个
  const dashboardIndex = children.findIndex((item:any) => item.path === "/dashoard");

  if (dashboardIndex !== -1 && dashboardIndex !== 0) {
    const dashItem = children[dashboardIndex]
    children.unshift(dashItem)
    children.splice(dashboardIndex+1,1)
  }



const router = createRouter({
  history: createWebHistory('/'),
  routes: [
    {
      path: '/',
      redirect:'/dashoard/newData',
      name: 'admin',
      component: ()=>import('@/views/admin/index.vue'),
      children
    },
    {
      path: '/login',
      component: () => import('@/views/login/index.vue'),
      meta: {
        nologin: true,
        menu: ''
      }
    }
  ]
})
router.beforeEach(checkUserLogin)

export default router

/*
 * @Author: cunFox
 * @Date: 2023-11-18 09:38:33
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-20 19:24:29
 */

import type { RouteRecordRaw } from "vue-router";
import { Document } from "@element-plus/icons-vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/informationProduct",
    redirect: "/informationProduct/info",
    meta: {
      menu: "体检信息管理",
    },
    children: [
      {
        path: "appointment",
        component: () => import("@/views/cunfox_appointment/index.vue"),
        meta: {
          breadcrumbs: [
            { title: "预约信息查询" },
            {
              to: "/informationProduct/cunfox_appointment",
              title: "预约信息查询",
            },
          ],
          menu: "预约信息查询",
        },
        props: (route) => {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10,
          };
        },
      },
      {
        path: "organization",
        component: () => import("@/views/cunfox_organization/index.vue"),
        meta: {
          breadcrumbs: [
            { title: "体检机构管理" },
            {
              to: "/informationProduct/cunfox_organization",
              title: "体检机构管理",
            },
          ],
          menu: "体检机构管理",
        },
        props:(route)=> {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10
          }
        }
      },
    ],
  },
];

export default routes;

import type { RouteRecordRaw } from 'vue-router'
import { Document } from '@element-plus/icons-vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/GroupInspection',
    redirect:'/GroupInspection/tuanjian2',
    meta: {
      menu: '团检管理',
    },
    children: [
      {
        path: 'tuanjianxinxiguanli',
        component: () => import('@/views/GroupInspection/tuanjianxinxiguanli/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '团检信息管理' },
            { to: '/commodityProduct/exPackage', title: '团检信息管理' }
          ],
          menu: '团检信息管理',
        },
        props: route => {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10
          }
        }
      },
      {
        path: 'tuanjian2',
        component: () => import('@/views/GroupInspection/tuanjian2/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '团检详情管理' },
            { to: '/commodityProduct/exItems', title: '团检详情管理' }
          ],
          menu: '团检详情管理',
        },
        props: route => {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10
          }
        }
      }
    ]
  }
]

export default routes

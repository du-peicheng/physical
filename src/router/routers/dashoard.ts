import type { RouteRecordRaw } from 'vue-router'
import { Document } from '@element-plus/icons-vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/dashoard',
    component: () => import('@/views/dashoard/index.vue'),
    redirect: '/dashoard/newData',
    meta: {
      menu: '首页',
      icon:Document
    },
    children: [
      {
        path: 'newData',
        component: () => import('@/views/dashoard/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '后台' },
            { to: '/dashoard/newData', title: '后台首页' }
          ],
          menu: '最新数据',
        }
      }
    ]

  }
]
export default routes

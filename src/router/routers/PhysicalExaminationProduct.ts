import type { RouteRecordRaw } from 'vue-router'
import { Document } from '@element-plus/icons-vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/commodityProduct',
    redirect:'/commodityProduct/exPackage',
    meta: {
      menu: '体检套餐管理',
    },
    children: [
      {
        path: 'exPackage',
        component: () => import('@/views/exPackage/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '体检套餐管理' },
            { to: '/commodityProduct/exPackage', title: '体检套餐管理' }
          ],
          menu: '体检套餐管理',
        }
      },
      {
        path: 'exItems',
        component: () => import('@/views/exItems/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '体检项管理' },
            { to: '/commodityProduct/exItems', title: '体检项管理' }
          ],
          menu: '体检项管理',
        }
      }
    ]
  }
]

export default routes

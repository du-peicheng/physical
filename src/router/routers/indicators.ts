import type { RouteRecordRaw } from 'vue-router'
import { Document } from '@element-plus/icons-vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/indicators',
    redirect:'/indicators/standard',
    meta: {
      menu: '体检指标管理',
    },
    children: [
      {
        path: 'institution',
        component: () => import('@/views/indicators/institution/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '机构指标管理' },
            { to: '/commodityProduct/exPackage', title: '机构指标管理' }
          ],
          menu: '机构指标管理',
        },
        props: route => {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10
          }
        }
      },
      {
        path: 'standard',
        component: () => import('@/views/indicators/standard/index.vue'),
        meta: {
          breadcrumbs: [
            { title: '标准指标管理' },
            { to: '/commodityProduct/exItems', title: '标准指标管理' }
          ],
          menu: '标准指标管理',
        },
        props: route => {
          return {
            page: route.query.page ? Number(route.query.page) : 1,
            size: route.query.size ? Number(route.query.size) : 10
          }
        }
      }
    ]
  }
]

export default routes

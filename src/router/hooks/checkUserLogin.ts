import type { RouteLocationNormalized } from 'vue-router'
import useUserStore from '@/stores/user'

// 检查当前用户访问的目标页面它是否要求只能登录才能访问
export default (to: RouteLocationNormalized) => {
  const userStore = useUserStore()
  // 只验证哪些需要登录才能访问的页面
  if (!to.meta.nologin) {
    if (!userStore.userinfo.token) {
      // 表示当前用户它没有登录
      return { path: '/login', replace: true }
    }else{
      // 验证 token 是否过期
      // 过期就退出登录
      if(userStore.userinfo.expirationTime && userStore.userinfo.expirationTime <=Date.now()){
        userStore.logoutUserInfo()
      }
    }
  }
}

// // 自定义的钩子函数，用于在数据读取时检查过期时间
// function afterCreate() {
//   const storedData = window.sessionStorage.getItem('user');
//   if (storedData) {
//     const userData = JSON.parse(storedData);
//     if (userData.expirationTime && userData.expirationTime <= Date.now()) {
//       // 数据已过期，执行相应处理
//       const userStore = useUserStore()
//       userStore.logoutUserInfo()
//     }
//   }
// }
import type { App } from 'vue'

import situationTitle from './situationTitle'

import fileImport from './fileImport'

export default (app: App) => {
  app.component('situationTitle', situationTitle)
  app.component('FileImport', fileImport)
}
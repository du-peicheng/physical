import axios from 'axios'

// 接口泛型来完成对于所有的请求返回值的二次封装
export interface IData<T> {
  code: number
  msg: string
  data: T
}


const ins = axios.create({
  timeout: 10000,
  baseURL: ''
})


// 响应拦截器
ins.interceptors.response.use(res => {
  return res.data
}, err => {
  return Promise.reject(err)
})


// export const post = <T>(url: string, data: any, config = {}) => ins.post<IData<T>>(url, data, config)
/**
 * Post请求
 * @param url 请求的url地址
 * @param data 请求体数据
 * @param config 请求配置 -- 额外请求
 * @returns Promise<T> 
 */
export const post = <T>(url: string, data: any, config = {}) => ins.post<any, IData<T>>(url, data, config)

/**
 * Put请求
 * @param url 请求的url地址
 * @param data 请求体数据
 * @param config 请求配置 -- 额外请求
 * @returns Promise<T> 
 */
export const put = <T>(url: string, data: any, config = {}) => ins.put<any, IData<T>>(url, data, config)


/**
 * Get请求
 * @param url 请求地址
 * @param config 请求配置 -- 额外请求
 * @returns Promise<T>
 */
export const get = <T>(url: string, config = {}) => ins.get<any, IData<T>>(url, config)
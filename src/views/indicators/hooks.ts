import { ref, onMounted } from 'vue'
import type { Ref } from 'vue'
import { standardListApi } from '@/services/standardApi'
import { institutionListApi } from '@/services/institutionApi'


export const useDialogVisible = (visible = false): [Ref<boolean>, (vis: boolean, cb?: () => void) => void] => {
  const data = ref(false)
  const setData = (vis: boolean, cb: () => void = () => { }) => {
    data.value = vis
    cb()
  }
  return [data, setData]
}


// 标准指标列表数据
export const useStandardList = (isStandard:boolean,pageNum = 1, query?: StandardMoudle.IQueryStandard, size = 10):
  [Ref<StandardMoudle.IStandardInfo[]>, Ref<number>, (page?: number, query?: StandardMoudle.IQueryStandard, size?: number) => Promise<void>] => {
  if (query) size = query?.size as number
  query = {}
  const data = ref<StandardMoudle.IStandardInfo[]>([])
  const total = ref(0)
  const loadData = async (page = 1, query?: StandardMoudle.IQueryStandard, size?: number, searchData?: StandardMoudle.IStandardInfo[]) => {
    if (searchData) {
      data.value = searchData
      total.value = searchData.length
    }
    else {
      let ret = await (isStandard? standardListApi(page, query, size):institutionListApi(page, query, size))
      if (ret.code === 0) {
        data.value = ret.data.Standards
        total.value = ret.data.total
      }
    }
    // console.log(data.value)
  }

  onMounted(() => {
    loadData(pageNum, query, size)
  })

  return [data, total, loadData]
}
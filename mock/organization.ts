/*
 * @Author: cunFox
 * @Date: 2023-11-20 17:59:27
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-21 22:38:41
 */

import Mock from "mockjs";

export default [
  {
    // 列表数据
    url: "/api/organization/organList",
    method: "get",
    response: (query) => {
      let page = query.page || 1;
      let size = query.size || 1;
      return {
        code: 0,
        msg: "ok",
        data: {
          page,
          size,
          total: 400,
          ...Mock.mock({
            "tableData|10": [
              {
                "organId|+1": 100001,
                "organName|1": ["瑞慈", "南医三院", "美年大健康", "慈铭体检"],
                "organBranch|1": ["天府广场", "环球中心", "春熙路"],
                city: "成都市",
                "organType|1": ["专门体检机构", "三甲医院",'二甲医院'],
                tel: "020-88888888",
                "enable|1": ["0", "1"],
                "type|1": ["0", "1"],
                'organUrl|1':["天府广场", "环球中心", "春熙路"],
                northern:'231',
                east:'123',
                capitalsName:'四川省',
                avatar:''
              },
            ],
          }),
        },
      };
    },
  },
  // 选项数据
  {
    url: "/api/organization/typeData",
    method: "get",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: {
          capital: [
            {
              id: 101,
              region: "北京市",
            },
            {
              id: 102,
              region: "山西省",
            },
            {
              id: 103,
              region: "四川省",
            },
            {
              id: 104,
              region: "河北省",
            },
          ],
          city: [
            {
              cid: 101,
              cities: ["海淀区","朝阳区", "通州区",  "昌平区"],
            },
            {
              cid: 102,
              cities: ["太原市", "大同市", "晋中市"],
            },
            {
              cid: 103,
              cities: ["成都市", "广安市", "乐山市"],
            },
            {
              cid: 104,
              cities: ["石家庄市", "唐山市", "邯郸市"],
            },
          ],
          organ: [
            {
              key: "三甲医院",
              value: "三甲医院",
              label: "三甲医院",
            },
            {
              key: "二甲医院",
              value: "二甲医院",
              label: "二甲医院",
            },
            {
              key: "专门体检机构",
              value: "专门体检机构",
              label: "专门体检机构",
            },
          ],
        },
      };
    },
  },
  // 查询数据
  {
    url: "/api/organization/search",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
  // 删除数据
  {
    url: "/api/organization/delete",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
   // 上传头像
   {
    url: '/api/organization/uploadOrganAvatar',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          url: 'http://video.1314000.cn/pic.png',
          path: './uploads/user/pic.jpg'
        }
      }
    }
  },

  // 删除上传的头像
  {
    url: '/api/organization/deleteOrganAvatar',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 添加数据
  {
    url: '/api/organization/addData',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 添加数据
  {
    url: '/api/organization/alterData',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 添加离源号
  {
    url: '/api/organization/addSource',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 获取离源号
  {
    url: '/api/organization/MessageSource',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          source:[
            {
              date:'2023-11-19',
              leisure:95,
              busyness:25
            },
            {
              date:'2023-11-12',
              leisure:100,
              busyness:74
            },
            {
              date:'2023-11-14',
              leisure:88,
              busyness:20
            },
            {
              date:'2023-11-11',
              leisure:96,
              busyness:54
            },
          ]
        }
      }
    }
  },
];

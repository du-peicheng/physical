import Mock from 'mockjs'

// mock 标准指标数据
export default [
  // 添加标准指标
  {
    url: '/api/standard/add',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 标准指标数据列表
  {
    url: '/api/standard/list',
    method: 'get',
    response: ({ query }) => {
      let page = query.page || 1
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...Mock.mock({
            "total|50-500": 50
          }),
          ...Mock.mock({
            // 属性 Standards 的值是一个数组，其中含有 10 个元素
            [`Standards|${query.size}`]: [{
              // 属性 id 是一个自增数，起始值为 1，每次增 1
              'id|+1': page * 10,
              'standardID|+1': [
                "BZZB000"+`${page * 10+1}`,
                "BZZB000"+`${page * 10+2}`,
                "BZZB000"+`${page * 10+3}`,
                "BZZB000"+`${page * 10+4}`, 
                "BZZB000"+`${page * 10+5}`,
                "BZZB000"+`${page * 10+6}`,
                "BZZB000"+`${page * 10+7}`,
                "BZZB000"+`${page * 10+8}`,
                "BZZB000"+`${page * 10+9}`,
                "BZZB000"+`${page * 10+10}`
              ],
              'standardName|1': [
                "血压",
                "心率",
                "血红蛋白",
                "白细胞计数",
                "血小板计数",
                "血糖",
                "肌酐",
                "尿酸",
                "甲状腺素",
                "总胆固醇",
                "甘油三酯",
                "高密度脂蛋白胆固醇",
                "低密度脂蛋白胆固醇",
                "血清蛋白",
                "血清钙",
                "血清镁",
                "血清钾",
                "血清钠",
                "血清铁",
                "血清锌",
                "血清铜",
                "血清维生素D",
                "血清维生素B12",
                "血清叶酸",
                "尿常规",
                "大便潜血",
              ],
              "unit|1": [
                'mmHg',
                'mmol/L',
                'mg/dL',
                'μmol/L',
                'mg/L',
                'ng/mL',
                'mg/mL'
              ],
              "upperLimit|600-3000": 600,
              "lowerLimit|1-300": 300,
              "normalPrompt|1": ['正常', ''],
              highPrompt: '',
              lowPrompt: '',
              'isComparable|1': true
            }]
          })
        }
      }
    }
  },
  // 查询指标
  {
    url: '/api/standard/search',
    method: 'get',
    response: ({ query }) => {
      let data = {}
      if (query.name) {
        data = {
          ...Mock.mock({
            // 属性 Standards 的值是一个数组，其中含有 10 个元素
            Standards: [{
              // 属性 id 是一个自增数，起始值为 1，每次增 1
              'id|+1': 1,
              'standardID|+1': [
                "BZZB00001",
                "BZZB00002",
                "BZZB00003",
                "BZZB00004",
                "BZZB00005",
                "BZZB00006",
                "BZZB00007",
                "BZZB00008",
                "BZZB00009",
                "BZZB00010",
              ],
              standardName: query.name,
              "unit|1": [
                'mmHg',
                'mmol/L',
                'mg/dL',
                'μmol/L',
                'mg/L',
                'ng/mL',
                'mg/mL'
              ],
              "upperLimit|600-3000": 600,
              "lowerLimit|1-300": 300,
              "normalPrompt|1": ['正常', ''],
              highPrompt: '',
              lowPrompt: '',
              isComparable: query.is === '1' ? true : false,
            }]
          })
        }
      } else {
        data = {
          ...Mock.mock({
            // 属性 Standards 的值是一个数组，其中含有 10 个元素
            'Standards|2-6': [{
              // 属性 id 是一个自增数，起始值为 1，每次增 1
              'id|+1': 1,
              'standardID|+1': [
                "BZZB00001",
                "BZZB00002",
                "BZZB00003",
                "BZZB00004",
                "BZZB00005",
                "BZZB00006",
                "BZZB00007",
                "BZZB00008",
                "BZZB00009",
                "BZZB00010",
              ],
              'standardName|1': [
                "血压",
                "心率",
                "血红蛋白",
                "白细胞计数",
                "血小板计数",
                "血糖",
                "肌酐",
                "尿酸",
                "甲状腺素",
                "总胆固醇",
                "甘油三酯",
                "高密度脂蛋白胆固醇",
                "低密度脂蛋白胆固醇",
                "血清蛋白",
                "血清钙",
                "血清镁",
                "血清钾",
                "血清钠",
                "血清铁",
                "血清锌",
                "血清铜",
                "血清维生素D",
                "血清维生素B12",
                "血清叶酸",
                "尿常规",
                "大便潜血",
              ],
              "unit|1": [
                'mmHg',
                'mmol/L',
                'mg/dL',
                'μmol/L',
                'mg/L',
                'ng/mL',
                'mg/mL'
              ],
              "upperLimit|600-3000": 600,
              "lowerLimit|1-300": 300,
              "normalPrompt|1": ['正常', ''],
              highPrompt: '',
              lowPrompt: '',
              isComparable: query.is === '1' ? true : false,
            }]
          })
        }
      }
      return {
        code: 0,
        msg: 'ok',
        data: data
      }
    }
  },
  // 批量删除
  {
    url: '/api/standard/deleteAllstandard',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 单个删除
  {
    url: '/api/standard/deleteStandard',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 数据回显
  {
    url: '/api/standard/:id',
    method: 'get',
    response: ({ query }) => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: query.id,
          'standardID|+1': [
            "BZZB00001",
            "BZZB00002",
            "BZZB00003",
            "BZZB00004",
            "BZZB00005",
            "BZZB00006",
            "BZZB00007",
            "BZZB00008",
            "BZZB00009",
            "BZZB00010",
          ],
          'standardName|1': [
            "血压",
            "心率",
            "血红蛋白",
            "白细胞计数",
            "血小板计数",
            "血糖",
            "肌酐",
            "尿酸",
            "甲状腺素",
            "总胆固醇",
            "甘油三酯",
            "高密度脂蛋白胆固醇",
            "低密度脂蛋白胆固醇",
            "血清蛋白",
            "血清钙",
            "血清镁",
            "血清钾",
            "血清钠",
            "血清铁",
            "血清锌",
            "血清铜",
            "血清维生素D",
            "血清维生素B12",
            "血清叶酸",
            "尿常规",
            "大便潜血",
          ],
          "unit|1": [
            'mmHg',
            'mmol/L',
            'mg/dL',
            'μmol/L',
            'mg/L',
            'ng/mL',
            'mg/mL'
          ],
          "upperLimit|600-3000": 600,
          "lowerLimit|1-300": 300,
          "normalPrompt|1": ['正常', ''],
          highPrompt: '',
          lowPrompt: '',
          'isComparable|1':  true
        }
      }
    }
  },
  // 根据返回数据修改
  {
    url: '/api/standard/:id',
    method: 'put',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 导入excel表数据
  {
    url: '/api/standard/adds',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
]
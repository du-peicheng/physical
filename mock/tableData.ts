import Mock from 'mockjs'

export default [
  // 体检套餐数据
  {
    url: '/api/table/data',
    method: 'get',
    response: ({ query }) => {
      let page = parseInt(query.page) || 1
      let size = parseInt(query.size) || 10  // 每页数据条数
      let total = 100   // 总数据条数
      let start = (page - 1) * size
      let end = start + size
  
      let tableData = Mock.mock({
        [`data|${size}`]: [
          {
            id: '@string("number", 8)',
            name: '@ctitle(10, 20)',
            oldPrice: '@integer(800, 1500)',
            sellingPrice: '@integer(600, 1200)',
            cities: ['北京'],
            enable: '启用',
            quantity: '@integer(500, 1500)'
          }
        ]
      }).data
  
      return {
        code: 0,
        msg: 'ok',
        data: {
          total: total,
          table: tableData.slice(start, end)
        }
      }
    }
  },
  // 体检项数据
  {
    url: '/api/tableproject/data',
    method: 'get',
    response: ({ query }) => {
      let page = parseInt(query.page) || 1
      let size = parseInt(query.size) || 10  // 每页数据条数
      let total = 100   // 总数据条数
      let start = (page - 1) * size
      let end = start + size
  
      let tableData = Mock.mock({
        [`data|${size}`]: [
          {
            id: '@string("number", 8)',
            name: '@ctitle(10, 20)',
            oldPrice: '@integer(10, 500)',
            sellingPrice: '@integer(20, 400)',
            cities: ['北京'],
            enable: '启用',
            quantity: '@integer(500, 1500)'
          }
        ]
      }).data
  
      return {
        code: 0,
        msg: 'ok',
        data: {
          total: total,
          table: tableData.slice(start, end)
        }
      }
    }
  },
//   删除套餐
  {
    url: '/api/table/del',
    method: 'post',
    response:()=>{
        return{
            code:0,
            msg:'ok',
            data:1
        }
    }
  },
//   删除体检项
  {
    url: '/api/table/delpro',
    method: 'post',
    response:()=>{
        return{
            code:0,
            msg:'ok',
            data:1
        }
    }
  },
  // 新增弹框中体检项数据
  {
    url: '/api/table/project',
    method: 'get',
    response: () => {
      const data = Mock.mock({
        'data|5': [{
          'id|+1': 1,
          name: '@ctitle',
          scope: '身高，体重，血压，脉搏',
          significance: '了解身体一般状况',
          labels: '不区分性别',
          'oldprice|100-1000': 1,
          'sellingPrice|100-1000': 1
        }]
      });
  
      return {
        code: 0,
        msg: 'ok',
        data: data.data
      };
    }
  },
  // 提交体检套餐
  {
    url:'/api/table/addpackage',
    method:'post',
    response:()=>{
      return{
        code:0,
        msg:'ok',
        data:1
      }
    }
  },
  // 提交体检项
  {
    url:'/api/table/addproject',
    method:'post',
    response:()=>{
      return{
        code:0,
        msg:'ok',
        data:1
      }
    }
  },
  // 批量删除体检套餐
  {
    url:'/api/table/removepackages',
    method:'post',
    response:()=>{
      return{
        code:0,
        msg:'ok',
        data:1
      }
    }
  },
  // 批量删除体检项
  {
    url:'/api/table/removeprojects',
    method:'post',
    response:()=>{
      return{
        code:0,
        msg:'ok',
        data:1
      }
    }
  },
  // 回显
  {
    url:'/api/table/echoData',
    method:'post',
    response:()=>{
      return{
          name:'aaa',
          province:'全部',
          cities:'全部',
          id:'54231456',
          institutions:'美宝',
          enable:true,
          branch:['全部'] as Array<string>,
          labels:['男性套餐'],
          date:'',
          oldprice:1000,
          newprice:800,
          introduce:'大大里发噶时光看来还是赶紧开始给',
          userules:'攻击力认识几个石老人国家',
          composition:'给i极乐世界格拉斯哥',
          projects:[{id
            : 
            2
            ,labels
            : 
            "不区分性别"
            ,name
            : 
            "法社军了"
            ,oldprice
            : 
            220
            ,scope
            : 
            "身高，体重，血压，脉搏"
            ,sellingPrice
            : 
            363
            ,significance: "了解身体一般状况"}] as Array<any>
      }
    }
  }
]
/*
 * @Author: cunFox
 * @Date: 2023-11-18 09:44:17
 * @Last Modified by: cunFox
 * @Last Modified time: 2023-11-21 16:13:02
 */

import Mock from "mockjs";

export default [
  // 用户信息列表
  {
    url: "/api/appointment/infoList",
    method: "get",
    response: (query) => {
      let page = query.page || 1;
      let size = query.size || 10;
      return {
        code: 0,
        msg: "ok",
        data: {
          page,
          size,
          total: 400,
          ...Mock.mock({
            "tableData|10": [
              {
                "id|+1": 1001,
                name: "@cname",
                "sex|1": ["1", "2"],
                tel: "13992638112",
                nId: '@ID',
                content: "4231套餐A",
                organ: "4421南医三院",
                date: "@date",
                "orderId|+1": 1000001,
                "state|1": [
                  "待支付",
                  "已过期",
                  "已体检",
                  "申请退款中",
                  "退款中",
                  "已退款",
                  "已取消",
                ],
              },
            ],
          }),
        },
      };
    },
  },
  // 状态信息选项
  {
    url: "/api/appointment/options",
    method: "get",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: {
          options: [
            {
              label: "全部",
            },
            {
              label: "待支付",
            },
            {
              label: "已过期",
            },
            {
              label: "已体检",
            },
            {
              label: "申请退款中",
            },
            {
              label: "退款中",
            },
            {
              label: "已退款",
            },
            {
              label: "已取消",
            },
          ],
        },
      };
    },
  },

  // 获取查询后的列表
  {
    url: "/api/appointment/search",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },

  // 修改用户状态
  {
    url: "/api/appointment/state",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
  // 批量修改用户状态
  {
    url: "/api/appointment/states",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
  // 修改用户时间
  {
    url: "/api/appointment/setDate",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
  // 上传文件
  
  {
    url: "/api/appointment/addFile",
    method: "post",
    response: () => {
      return {
        code: 0,
        msg: "ok",
        data: 1,
      };
    },
  },
];

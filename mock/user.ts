import Mock from 'mockjs'

// mock 用户数据
export default[
    // 用户登录
    {
        url:'/api/user/login',
        method:'post',
        response:()=>{
            return{
                code:0,
                msg:'ok',
                data:{
                    userid:1,
                    name:'坤坤',
                    token:'rtreghdhdgfhgdfhgdfhdhdfggdgfdhtjkbncvx',
                }
            }
        }
    },
    // 退出登录
    {
        url:'/api/user/logout',
        method:'get',
        response:()=>{
            return{
                code:0,
                msg:'ok',
                data:1
            }
        }
    }
]
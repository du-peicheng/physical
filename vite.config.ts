import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// mock数据服务
import { viteMockServe } from 'vite-plugin-mock'

export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    // 自动导入对应的模块
    AutoImport({
      // ts需要dts选项
      dts: true,
      imports: ['vue', 'vue-router'],
      resolvers: [ElementPlusResolver()]
    }),
    // 导入组件并引入css
    Components({
      resolvers: [ElementPlusResolver()]
    }),
    viteMockServe()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    host: '0.0.0.0',
    // 打开浏览器
    // open: true
  }
})
